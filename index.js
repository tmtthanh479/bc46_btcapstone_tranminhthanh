var checkBox_toggle=document.getElementById("checkbox");
console.log(checkBox_toggle);
checkBox_toggle.addEventListener("change",function(){
    document.body.classList.toggle('dark')
})

let valuedisplay = document.querySelectorAll(".num");
let interval = 4000;

// valuedisplay.forEach((valuedisplay) =>{
//     let startValue = 0;
//     let endValue = parseInt(valuedisplay.getAttribute("data-val"));
//     let duration = Math.floor(interval/endValue);
//     let counter = setInterval(function(){
//         startValue += 1;
//         valuedisplay.textContent = startValue;
//         if ( startValue == endValue){
//             clearInterval(counter);
//         }
//     })
// });

$('.counter').countUp({
    'time': 2000,
    'delay': 10
  });

// animation video
$('#play-video').on('click', function(e){
  e.preventDefault();
  $('#video-overlay').addClass('open');
  $("#video-overlay").append('<iframe width="560" height="315" src="https://player.vimeo.com/video/305185528" frameborder="0" allowfullscreen></iframe>');
});

$('.video-overlay, .video-overlay-close').on('click', function(e){
  e.preventDefault();
  close_video();
});

$(document).keyup(function(e){
  if(e.keyCode === 27) { close_video(); }
});

function close_video() {
  $('.video-overlay.open').removeClass('open').find('iframe').remove();
};